#include "generic.pb.h"
#include "server/Connection.hpp"
#include "server/World.hpp"

#include <iostream>
#include <boost/asio.hpp>



int main() {
    try {
        boost::asio::io_context ioc;
        Connection::Server server(ioc);
        int port = 78432;
        boost::asio::co_spawn(ioc, server.listen(port), boost::asio::detached);
        std::cout << "Server has initialized and will now start listening on port " << port << '.' << std::endl;
        ioc.run();
    } catch (std::exception& e) {
        std::cerr << "Failed to launch: " << e.what() << std::endl;
    }
}
